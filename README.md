# ESP_LED_light_control

Controlling LED strips by MQTT and buttons. Trying to achieve a failsafe design.

A description of the project can be found on [hackaday.io](https://hackaday.io/project/183343-esp-led-light-control).

Two config files need to be provided:

secret_config.h
```
// secret_config.h
// WIFI configuration
#define WIFI_SSID "foo"
#define WIFI_PASSWORD "gizmo"
```
config.h
```
// config.h
// MQTT Broker
#define MQTT_SERVER "192.168.2.202"  // IP where the MQTT broker is
#define MQTT_PORT 1883               // the port of the MQTT broker

// ESP's IP and network details
IPAddress local_ip(192, 168, 2, 160);	// I prefer to have fixed IPs (usually the ESP code runs faster)
IPAddress gateway(192,168,2,1);         // router IP
IPAddress subnet(255,255,255,0);
```
Both need to be adapted as necessary.

I know that there are libraries such as [IotWebConf](https://github.com/prampec/IotWebConf) or [WiFiManager](https://github.com/tzapu/WiFiManager). However, I do not need to have always WiFi access or that the ESP switches to AP mode when it looses WiFi connection. I just want the device keeping its basic functionality (buttons to turn on/off) if WiFi is lost, i.e. searching for WiFi not to block the device. Maybe there are more elegant 
solutions possible. I just want one that works.

