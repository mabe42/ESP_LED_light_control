#include <ESP8266WiFi.h>
#include "secret_config.h" // WiFi SSID, Password
#include "config.h"  // general configs MQTT,...
#include <PubSubClient.h> //For MQTT

// #define DEBUG_ESP_PORT Serial
#ifdef DEBUG_ESP_PORT
#define DEBUG_MSG(...) DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
#define DEBUG_MSG(...)
#endif // see https://arduino-esp8266.readthedocs.io/en/latest/Troubleshooting/debugging.html


const uint16_t pwmtable_10[64] =
{
    0, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 9, 10,
    11, 12, 13, 15, 17, 19, 21, 23, 26, 29, 32, 36, 40, 44, 49, 55,
    61, 68, 76, 85, 94, 105, 117, 131, 146, 162, 181, 202, 225, 250,
    279, 311, 346, 386, 430, 479, 534, 595, 663, 739, 824, 918, 992
}; // array from http://www.mikrocontroller.net/articles/LED-Fading to take into account
// the logarithmic sensitivity of the eye
// 992 instead of 1023 to prevent LED from wearing out too quickly

#define MAX_BRIGHT 100 // possibility to further reduce maximum brightness
byte ledPin[] = {12,13,14}; // {GPIO12, GPIO13, GPIO14} = Wemos D1 mini {D6,D7,D5}
byte tastPin[] = {4,0,5};  // {GPIO4, GPIO0, GPIO5} = Wemos D1 mini {D2,D3,D1}
volatile uint16_t ledLevel[] = {0,0,0};
volatile bool ledState[] = {false,false,false};
volatile bool pressed[] = {false,false,false};
long lastmillis;

volatile long last_wifi_attempt;
volatile byte failed_wifi_attempts = 0;

#define WIFI_ATTEMPT_DELAY 300000  // retry after 5 mins
#define MAX_WIFI_ATTEMPTS 144 // reboot after 12h without WiFi

//MQTT client
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

volatile long last_mqtt_attempt;
volatile byte failed_mqtt_attempts = 0;

#define MQTT_ATTEMPT_DELAY 300000  // retry after 5 mins
#define MAX_MQTT_ATTEMPTS 12 // reboot after 1h without MQTT

//volatile uint16_t poti; // see in may loop
//uint16_t val;
//uint16_t frac;

//#define POTI_LED 0
  
#define FADE_TIME 50  // delay in ms between the fading steps

void wifi_connect() {
  // try 10 times per connection attempt
  byte current_tries = 0;
  #define MAX_CURRENT_TRIES 10
  //delay(10);
  WiFi.mode(WIFI_STA);
// static IP defined in config.h
  WiFi.config(local_ip,gateway,subnet);
  DEBUG_MSG("Connecting to %s\n", WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while ((WiFi.status() != WL_CONNECTED) && (current_tries < MAX_CURRENT_TRIES)) {
    for (byte i=0; i<2; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(50);
      digitalWrite(LED_BUILTIN, LOW);
      delay(150);
    }
    delay(200);
    DEBUG_MSG(".");
    current_tries++;
  }
  DEBUG_MSG("Wifi OK\n");
  if (current_tries >= MAX_CURRENT_TRIES) {
    failed_wifi_attempts++;
    last_wifi_attempt = millis();
    DEBUG_MSG("WiFi unsuccessful right now.\n");
    DEBUG_MSG("Total of %d unseccesuful attempts.\n", failed_wifi_attempts);
    if (failed_wifi_attempts == MAX_WIFI_ATTEMPTS) {
      DEBUG_MSG("MAX_WIFI_ATTEMPTS reached. Rebooting.\n");
      delay(500);
      ESP.restart();
    }
  }
  DEBUG_MSG("Failed WiFi attempts: %d\n", failed_wifi_attempts);  
}

void mqtt_reconnect() {
  byte current_mtries = 0;
  #define MAX_CURRENT_MTRIES 5
// Loop until we're reconnected
  while ((!mqttClient.connected()) && (current_mtries < MAX_CURRENT_MTRIES)) {
    DEBUG_MSG("  Need to reconnect to MQTT :-( ");
// Attempt to connect
    if (mqttClient.connect(mqttId)) {
//      digitalWrite(LED_BUILTIN, LOW);
      DEBUG_MSG(" ...connected\n");
      delay(100);
//      digitalWrite(LED_BUILTIN, HIGH);
      current_mtries++;
      for (byte i=0; i<3; i++) {
        mqttClient.subscribe(ledTopic[i]);
      }
    } else {
      DEBUG_MSG("(re)connect failed, rc=%d \n", mqttClient.state());
      DEBUG_MSG(" try again in 5 seconds\n");
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);
      // Wait 5 seconds before retrying
      delay(4500);
      current_mtries++;
    }
  }
  if (current_mtries >= MAX_CURRENT_MTRIES) {
    failed_mqtt_attempts++;
    last_mqtt_attempt = millis();
    DEBUG_MSG("MQTT unsuccessful.\n");
    DEBUG_MSG("Total of %d unseccesuful attempts.\n", failed_wifi_attempts);
    if (failed_mqtt_attempts == MAX_MQTT_ATTEMPTS) {
      DEBUG_MSG("MAX_MQTT_ATTEMPTS reached. Rebooting.\n");
      delay(500);
      ESP.restart();
    }
  }
  digitalWrite(LED_BUILTIN, HIGH);
  mqttClient.publish("ESP/Test/wac", String(failed_wifi_attempts).c_str(), true);
  mqttClient.publish("ESP/Test/mac", String(failed_mqtt_attempts).c_str(), true);  
  DEBUG_MSG("Failed MQTT attempts: %d\n", failed_mqtt_attempts);  
}

void callback(char* topic, byte* payload, unsigned int length) {
  int percent;
  char message_buf[255];
  DEBUG_MSG("New message arrived [%s] \n", topic);
  for (unsigned int i = 0; i < length; i++) {
    DEBUG_MSG("%c",(char)payload[i]);
    message_buf[i] = (char)payload[i];
  }
  message_buf[length] = '\0';
  DEBUG_MSG("message_buf: %s  ",&message_buf);
  percent = atoi(message_buf);
  DEBUG_MSG("Level: %d\n", percent);
  for (byte i=0; i<3; i++) {
    if (strcmp(topic, ledTopic[i]) == 0) {
      fadeLed(i, percent);
    }
  }
}

void fadeLed(byte ledNo, uint8_t ledPercent) {
  // map 0-100% to 0..63 for the index
  // see discussion on stackovrflow:
  // https://stackoverflow.com/questions/5731863/mapping-a-numeric-range-onto-another
  uint16_t index;
  if (ledPercent > MAX_BRIGHT)
    ledPercent = MAX_BRIGHT;
  if (ledPercent < 0)
    ledPercent = 0;
  index = (ledPercent * 63) / 100;
  DEBUG_MSG("LED, index %d\n", index);
  if (index > ledLevel[ledNo]) {
    // fade in
    for (int i=ledLevel[ledNo]; i<=index; i++) {
        analogWrite(ledPin[ledNo], pwmtable_10[i]);
        delay(FADE_TIME);
    }

    ledState[ledNo] = true;
    ledLevel[ledNo] = index;
    if (ledPercent == 100)
      analogWrite(ledPin[ledNo], pwmtable_10[63]);
  }
  else {
    // fade out
    for (int i=ledLevel[ledNo]; i>=index; i--) {
        analogWrite(ledPin[ledNo], pwmtable_10[i]);
        delay(FADE_TIME);
    }
    ledLevel[ledNo] = index;
    if (ledPercent == 0) {
      digitalWrite(ledPin[ledNo], 0);
      ledState[ledNo] = false;
    }
  }
}

ICACHE_RAM_ATTR void button0Pressed() {
  static unsigned long last_interrupt_time = 0;
  DEBUG_MSG("IRQ 0");
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 250ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 250)
  {
    pressed[0] = 1;
  }
  last_interrupt_time = interrupt_time;
}

ICACHE_RAM_ATTR void button1Pressed() {
  static unsigned long last_interrupt_time = 0;
  DEBUG_MSG("IRQ 1");
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 250ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 250)
  {
    pressed[1] = 1;
  }
  last_interrupt_time = interrupt_time;
}

ICACHE_RAM_ATTR void button2Pressed() {
  static unsigned long last_interrupt_time = 0;
  DEBUG_MSG("IRQ 2");
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 250ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 250)
  {
    pressed[2] = 1;
  }
  last_interrupt_time = interrupt_time;
}

void setup() {
  Serial.begin(115200);
  DEBUG_MSG("\nStarting...\n");
  DEBUG_MSG("OK\n");
  
  for (byte i=0; i<3; i++) {
    pinMode(ledPin[i], OUTPUT);
    digitalWrite(ledPin[i], 0);
    pinMode(tastPin[i], INPUT_PULLUP);
  }
  attachInterrupt(digitalPinToInterrupt(tastPin[0]), button0Pressed, FALLING);
  attachInterrupt(digitalPinToInterrupt(tastPin[1]), button1Pressed, FALLING);
  attachInterrupt(digitalPinToInterrupt(tastPin[2]), button2Pressed, FALLING);
  
  last_wifi_attempt = millis();
  DEBUG_MSG("Starting WiFi...");
  wifi_connect();
  if (WiFi.status() == WL_CONNECTED)
    DEBUG_MSG(" started.\n");
  else
    DEBUG_MSG(" failed.\n");
  
  mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
  mqttClient.setCallback(callback);
  if (!mqttClient.connected()) {
    mqtt_reconnect();
  }
  mqttClient.loop();
  
  //poti = analogRead(A0);

  DEBUG_MSG("Setup completed! Running...\n");
  lastmillis = millis();
}

void loop() {
  if ((WiFi.status() != WL_CONNECTED) && (millis()-last_wifi_attempt > WIFI_ATTEMPT_DELAY)) {
    DEBUG_MSG("Lost WiFi connection!\n");
    wifi_connect();
  }
  else {
    last_wifi_attempt = millis();
    // check for MQTT connection only if connected to WiFi
    if (!mqttClient.connected() && (millis() - last_mqtt_attempt > MQTT_ATTEMPT_DELAY)) {
      DEBUG_MSG("No MQTT connection!\n");
      mqtt_reconnect();
    }
    else {
      last_mqtt_attempt = millis();
      failed_mqtt_attempts = 0;
    }
    failed_wifi_attempts = 0;
  }

  for (byte i=0; i<3; i++) {
    if (pressed[i]) {
      pressed[i] = 0;
      DEBUG_MSG("Pressed %d: ", i);
      if (ledState[i]) // pressing the button toggles between 0 and MAX_BRIGHT
        fadeLed(i,0);
      else 
        fadeLed(i,MAX_BRIGHT);
    }
  }

// calling loop() too often or too rarely causes problems
  if ((millis() - lastmillis) > 100) {
    lastmillis = millis();
    mqttClient.loop();
  }
/* for some reason using 10k/50k/100k I only get val <= 165. There also seems to be some interference 
between A0 and Wifi when starting WiFi. Therefore no poti for the time being.  
  val = analogRead(A0);
  if ((val > (poti+10)) || (val < (poti-10))) {
    frac = map(val,0,1023,0,MAX_BRIGHT);
    fadeLed(POTI_LED, frac);
    poti = val;
    DEBUG_MSG("New Poti value: %d\n", val);
  }
  */
}

